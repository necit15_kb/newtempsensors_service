#include "main.h"

#define TMP117_I2C_ADDR ((uint32_t) 0x48) 	// I2C bus address TMP117 
#define BME280_I2C_ADDR ((uint32_t) 0x76)		// I2C bus address TMP117

extern uint8_t I2C_buf_tx[], I2C_buf_rx[];  // input and output buffers I2C

const double preciseTemp_coeff_Celsius = 0.78125; // coefficient for sensors TMP117

volatile static uint32_t delay_ms; // variable for delay in ms

uint8_t COMPLETE_FLAG = 0; 				// complete flag measuring

/* ----- Variables and functions needs for working BME280 sensor ----- */
BME280_S32_t t_fine;

u16 dig_T1;
s16 dig_T2;
s16 dig_T3;

u16 dig_P1;
s16 dig_P2;
s16 dig_P3;
s16 dig_P4;
s16 dig_P5;
s16 dig_P6;
s16 dig_P7;
s16 dig_P8;
s16 dig_P9;

u8 dig_H1;
s16 dig_H2;
u8 dig_H3;
s16 dig_H4;
s16 dig_H5;
s8 dig_H6;

u16 char_to_uint (char *a);
s16 char_to_sint (char *a);
s32 char_to_sint_d (char *a);
s32 char3_to_s32 (char *a);

/* --------------------------------------------------- */

mainService weatherService; // init main struct data
bme280_struct bme280;				// init struct BME280 data
tmp117_struct tmp117;				// init struct TMP117 data


void init_gpio(void); //encapsulated function MX_GPIO_Init() from main.c
void init_dma(void);  //encapsulated function MX_DMA_Init() from main.c
void init_i2c(void);  //encapsulated function MX_I2C_Init() from main.c

BME280_S32_t BME280_compensate_T_int32(int32_t t_fine); 								// function for precise measuring temperature BME280 sensor
BME280_U32_t BME280_compensate_P_int32(int32_t adc_P, int32_t t_fine);	// function for precise measuring pressure BME280 sensor
BME280_U32_t bme280_compensate_H_int32(int32_t adc_H, int32_t t_fine);  // function for precise measuring humidity BME280 sensor

void WeatherServiceFunction(void); 	// main function of service
void CleanBuf(void); 								// clear input and output I2C buffers




BME280_S32_t BME280_compensate_T_int32(int32_t t_fine){
	 int32_t T;
  T = (t_fine * 5 + 128) >> 8;
  return T;
}

BME280_U32_t BME280_compensate_P_int32(int32_t adc_P, int32_t t_fine)
{
		int32_t varP1, varP2;
		uint32_t P;
	
		varP1 = (t_fine>>1) - (int32_t)64000;
    varP2 = (((varP1>>2) * (varP1>>2)) >> 11 ) * ((int32_t)dig_P6);
    varP2 = varP2 + ((varP1*((int32_t)dig_P5))<<1);
    varP2 = (varP2>>2)+(((int32_t)dig_P4)<<16);
    varP1 = (((dig_P3 * (((varP1>>2) * (varP1>>2)) >> 13 )) >> 3) + ((((int32_t)dig_P2) * varP1)>>1))>>18; 
    varP1 = ((((32768+varP1))*((int32_t)dig_P1))>>15);
    if (varP1 == 0) 
    {
        return 0; 
    }
    P = (((uint32_t)(((int32_t)1048576)-adc_P)-(varP2>>12)))*3125; 
    if (P < 0x80000000)
    {
        P = (P << 1) / ((uint32_t)varP1); 
    }
    else
    {
        P = (P / (uint32_t)varP1) * 2;
    }
    varP1 = (((int32_t)dig_P9) * ((int32_t)(((P>>3) * (P>>3))>>13)))>>12; 
    varP2 = (((int32_t)(P>>2)) * ((int32_t)dig_P8))>>13;
    P = (uint32_t)((int32_t)P + ((varP1 + varP2 + dig_P7) >> 4));
    return P;
}

BME280_U32_t bme280_compensate_H_int32(int32_t adc_H, int32_t t_fine){ 
		int32_t varH;
  varH = (t_fine - ((int32_t)76800));
  varH = (((((adc_H << 14) - (((int32_t)dig_H4) << 20) - (((int32_t)dig_H5) * varH)) +
    ((int32_t)16384)) >> 15) * (((((((varH * ((int32_t)dig_H6)) >> 10) * (((varH *
    ((int32_t)dig_H3)) >> 11) + ((int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)dig_H2) + 8192) >> 14));
  varH = (varH - (((((varH >> 15) * (varH >> 15)) >> 7) * ((int32_t)dig_H1)) >> 4));
  varH = (varH < 0 ? 0 : varH); 
  varH = (varH > 419430400 ? 419430400 : varH);
  return(uint32_t)(varH >> 12);
} 

void WeatherServiceFunction(void){
	
	uint8_t Posr = P_OSR_16, Hosr = H_OSR_16, Tosr = T_OSR_02, Mode = normal, IIRFilter = BW0_021ODR, SBy = t_62_5ms; // set settings BME280
	int32_t result[5]; 					// additional output buffer 
	int32_t var1, var2, adc_T; 	// additional variables for BME280 sensor
	
	volatile static uint32_t my_tick; // variable for counting ms from is SysTick 
	volatile int32_t value = 0; 			// output variable for TMP117 
	
		if(weatherService.cmd != RST)
		{
			if(my_tick != uwTick)
			{
				my_tick = uwTick;

					if(COMPLETE_FLAG)
						{
							weatherService.cmd = KILL;
							COMPLETE_FLAG = 0;
						}
					switch (weatherService.cmd)
					{
					case START:
						LL_GPIO_ResetOutputPin(EN_SENS_GPIO_Port, EN_SENS_Pin);
						LL_I2C_Disable(I2C2);
						LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_4);
						LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_5);
						weatherService.cmd = RST;
					
						init_gpio();
						init_dma();
						init_i2c();
						LL_GPIO_SetOutputPin(EN_SENS_GPIO_Port, EN_SENS_Pin);
						delay_ms = 1000; 
						weatherService.step = 1;
						weatherService.cmd = DELAY;
						weatherService.state = DELAY;
					break;

					case DELAY:
						if(!delay_ms)
						{
							weatherService.state = RUNNING;
							weatherService.cmd = RUNNING;
							
						}
						delay_ms--;
						
					break;

					case RUNNING:
					
						switch (weatherService.step)
							{
							case 1:
								CleanBuf();
							
								DMA1_Channel4->CNDTR = 2; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);

								I2C_buf_tx[0] = BME280_CTRL_HUM;
								I2C_buf_tx[1] = 0x07 & Hosr;
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 2, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE); //DMA will be write in sensors
							break;

							case 2:
								CleanBuf();
								
								DMA1_Channel4->CNDTR = 2; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);

								I2C_buf_tx[0] = BME280_CTRL_MEAS;
								I2C_buf_tx[1] = Tosr << 5 | Posr << 2 | Mode;
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 2, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE); //DMA will be write in sensors
							break;

							case 3:
								CleanBuf();
							
								DMA1_Channel4->CNDTR = 2;
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);

								I2C_buf_tx[0] = BME280_CONFIG;
								I2C_buf_tx[1] = SBy << 5 | IIRFilter << 2;
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 2, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE); //DMA will be write in sensors
							
							break;

							case 4:
								CleanBuf();	
							
								DMA1_Channel4->CNDTR = 1; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4); 

								I2C_buf_tx[0] = BME280_CALIB00;
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 1, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE); //DMA will be write in sensors
							break;
							 
							case 5:
								CleanBuf();
							
								DMA1_Channel5->CNDTR = 27; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);

								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 27, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_READ); //DMA will be read out sensors
							break;
							 
							case 6:
								dig_T1 = (uint16_t)(((uint16_t) I2C_buf_rx[1] << 8) | I2C_buf_rx[0]);
								dig_T2 = ( int16_t)((( int16_t) I2C_buf_rx[3] << 8) | I2C_buf_rx[2]);
								dig_T3 = ( int16_t)((( int16_t) I2C_buf_rx[5] << 8) | I2C_buf_rx[4]);

								dig_P1 = (uint16_t)(((uint16_t) I2C_buf_rx[7] << 8) | I2C_buf_rx[6]);
								dig_P2 = ( int16_t)((( int16_t) I2C_buf_rx[9] << 8) | I2C_buf_rx[8]);
								dig_P3 = ( int16_t)((( int16_t) I2C_buf_rx[11] << 8) | I2C_buf_rx[10]);
								dig_P4 = ( int16_t)((( int16_t) I2C_buf_rx[13] << 8) | I2C_buf_rx[12]);
								dig_P5 = ( int16_t)((( int16_t) I2C_buf_rx[15] << 8) | I2C_buf_rx[14]);
								dig_P6 = ( int16_t)((( int16_t) I2C_buf_rx[17] << 8) | I2C_buf_rx[16]);
								dig_P7 = ( int16_t)((( int16_t) I2C_buf_rx[19] << 8) | I2C_buf_rx[18]);
								dig_P8 = ( int16_t)((( int16_t) I2C_buf_rx[21] << 8) | I2C_buf_rx[20]);
								dig_P9 = ( int16_t)((( int16_t) I2C_buf_rx[23] << 8) | I2C_buf_rx[22]);
						
								dig_H1 = I2C_buf_rx[25];
								
								CleanBuf();
							
								DMA1_Channel4->CNDTR = 1; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);

								I2C_buf_tx[0] = BME280_CALIB26;
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 1, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE); // DMA will be write in sensors
							break;
							
							case 7:
							
								DMA1_Channel5->CNDTR = 8; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
							
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 8, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_READ); // DMA will be read out sensors
							break;
							
							case 8:
								dig_H2 = ( int16_t)((( int16_t) I2C_buf_rx[1] << 8) | I2C_buf_rx[0]);
								dig_H3 = I2C_buf_rx[2];
								dig_H4 = ( int16_t)(((( int16_t) I2C_buf_rx[3] << 8) | (0x0F & I2C_buf_rx[4]) << 4) >> 4);
								dig_H5 = ( int16_t)(((( int16_t) I2C_buf_rx[5] << 8) | (0xF0 & I2C_buf_rx[4])) >> 4 );
								dig_H6 = I2C_buf_rx[6];
							
								DMA1_Channel4->CNDTR = 1; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_4);
							
								CleanBuf();
							
								I2C_buf_tx[0] = 0xF7;
								
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT,  1, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_WRITE);  // DMA will be write in sensors
								//DMA will be write in sensors
								delay_ms = 100;
								weatherService.cmd = DELAY;
								weatherService.state = DELAY;
							break;
							 
							case 9:
								
							
								CleanBuf();
							
								DMA1_Channel5->CNDTR = 9;
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
								 
								LL_I2C_HandleTransfer(I2C2, BME280_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 9, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_READ); // DMA will be read out sensors
							break;
							 
							case 10:
								
							
								result[0] = (uint32_t) (((uint32_t) I2C_buf_rx[0] << 16 | (uint32_t) I2C_buf_rx[1] << 8 | I2C_buf_rx[2]) >> 4);
								result[1] = (uint32_t) (((uint32_t) I2C_buf_rx[3] << 16 | (uint32_t) I2C_buf_rx[4] << 8 | I2C_buf_rx[5]) >> 4);
								result[2] = (uint16_t) (((uint16_t) I2C_buf_rx[6] <<  8 |            I2C_buf_rx[7]) );
									
								adc_T = result[1];
								
								var1 = (((( adc_T >> 3) - ((int32_t)dig_T1 << 1))) * ((int32_t)dig_T2)) >> 11;
								var2 = (((((adc_T >> 4) - ((int32_t)dig_T1)) * ((adc_T >> 4) - ((int32_t)dig_T1))) >> 12) * ((int32_t)dig_T3)) >> 14;
								
								t_fine = var1 + var2;
								
								weatherService.bme280.pressure = BME280_compensate_P_int32(result[0], t_fine);
								weatherService.bme280.temperature = BME280_compensate_T_int32(t_fine);
								weatherService.bme280.humidity = bme280_compensate_H_int32(result[2], t_fine);
							
								CleanBuf();
							
								DMA1_Channel5->CNDTR = 2; 
								LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);
								 
								LL_I2C_HandleTransfer(I2C2, TMP117_I2C_ADDR << 1, LL_I2C_ADDRSLAVE_7BIT, 2, LL_I2C_MODE_AUTOEND,LL_I2C_GENERATE_START_READ); // DMA will be read out sensors
							break;
							 
							case TMP_READ_DATA:
								value = (uint32_t)I2C_buf_rx[0];
								value <<= 8;
								value |= (uint32_t)I2C_buf_rx[1];
								weatherService.tmp117.temperature = (preciseTemp_coeff_Celsius * value); 
				
								weatherService.state = SUCCES;
								COMPLETE_FLAG = 1;
							break;
							
							default:
								break;
							}
					break;

					case KILL:
						LL_GPIO_ResetOutputPin(EN_SENS_GPIO_Port, EN_SENS_Pin);
						LL_I2C_Disable(I2C2);
						LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_4);
						LL_DMA_DeInit(DMA1, LL_DMA_CHANNEL_5);
						weatherService.cmd = RST;
					break;

					default:break;
					}
	}
}
}

void CleanBuf(void)
{
	int i;
	for(i = 0; i < SIZE_BUF; i++) // in main.h #define SIZE_BUF ((uint16_t)30)
	{
		I2C_buf_tx[i] = 0;
		I2C_buf_rx[i] = 0;
	}
}	



u16 char_to_uint (char *a){
	u16 b=0;
	b=*(a+1);
	b<<=8;
	b|=*a;
	return b;	
}
	
s16 char_to_sint (char *a){
	s16 b=0;
	b=*(a+1);
	b<<=8;
	b|=*a;
	return b;	
}

s32 char_to_sint_d (char *a){
	s32 b=0;
	b=*a;
	b<<=8;
	b|=*(a+1);
	return b;	
}

s32 char3_to_s32 (char *a){	
	s32 b=0;
	b=*a;
	b<<=8;
	b|=*(a+1);
	b<<=8;
	b|=*(a+2);
	return b;	
}
