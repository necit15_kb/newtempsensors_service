#ifndef __WEATHERSENSORS_SERVICE_H
#define __WEATHERSENSORS_SERVICE_H
#include "main.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BME280_CONFIG     0xF5
#define BME280_CTRL_MEAS  0xF4
#define BME280_STATUS     0xF3
#define BME280_CTRL_HUM   0xF2
#define BME280_RESET      0xE0
#define BME280_CALIB00    0x88
#define BME280_CALIB26    0xE1

enum Posr {P_OSR_00 = 0, /* no op */ P_OSR_01, P_OSR_02, P_OSR_04, P_OSR_08, P_OSR_16};
enum Hosr {H_OSR_00 = 0, /* no op */ H_OSR_01, H_OSR_02, H_OSR_04, H_OSR_08, H_OSR_16};
enum Tosr {T_OSR_00 = 0, /* no op */ T_OSR_01, T_OSR_02, T_OSR_04, T_OSR_08, T_OSR_16};
enum IIRFilter {full = 0,  /* bandwidth at full sample rate */ BW0_223ODR, BW0_092ODR, BW0_042ODR, BW0_021ODR /* bandwidth at 0.021 x sample rate */ };
enum Mode {BME280Sleep = 0, forced, forced2, normal};
enum SBy  {t_00_5ms = 0, t_62_5ms, t_125ms, t_250ms, t_500ms, t_1000ms, t_10ms, t_20ms};

typedef int32_t s32;
typedef int16_t s16;
typedef int8_t  s8;

typedef const int32_t sc32;  /*!< Read Only */
typedef const int16_t sc16;  /*!< Read Only */
typedef const int8_t sc8;   /*!< Read Only */

typedef __IO int32_t  vs32;
typedef __IO int16_t  vs16;
typedef __IO int8_t   vs8;

typedef __I int32_t vsc32;  /*!< Read Only */
typedef __I int16_t vsc16;  /*!< Read Only */
typedef __I int8_t vsc8;   /*!< Read Only */

typedef uint32_t  u32;
typedef uint16_t u16;
typedef uint8_t  u8;

typedef const uint32_t uc32;  /*!< Read Only */
typedef const uint16_t uc16;  /*!< Read Only */
typedef const uint8_t uc8;   /*!< Read Only */

typedef __IO uint32_t  vu32;
typedef __IO uint16_t vu16;
typedef __IO uint8_t  vu8;

typedef __I uint32_t vuc32;  /*!< Read Only */
typedef __I uint16_t vuc16;  /*!< Read Only */
typedef __I uint8_t vuc8;   /*!< Read Only */

typedef s32 BME280_S32_t;
typedef u32 BME280_U32_t;

typedef struct 
{
    s32 temperature;
    u32 pressure;
		u32 humidity;
} bme280_struct;
extern bme280_struct bme280;

typedef struct 
{
    int32_t temperature;
} tmp117_struct;
extern tmp117_struct tmp117;

typedef enum {RST = 0, START, DELAY, RUNNING, KILL, SUCCES} MainEnumState;
typedef enum {BME_WRITE_CALIB_1 = 0, BME_READ_CALIB_1, BME_WRITE_CALIB_2, BME_READ_CALIB_2, BME_read_grm, BME_WRITE_CALIB_3, BME_WRITE_CALIB_4, BME_WRITE_CALIB_7, DelayBeforeReadData, BME_WRITE_CALIB_5, BME_READ_DATA, TMP_READ_DATA} EnumStep;

typedef struct 
{
    tmp117_struct tmp117;
    bme280_struct bme280;
    MainEnumState cmd;
    EnumStep step;
    MainEnumState state;
}  mainService;
extern  mainService weatherService;

#endif
